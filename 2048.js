const canvas = document.getElementById('game_table');
const ctx = canvas.getContext('2d');
const sizeInput = document.getElementById('size_number');
const changeSize = document.getElementById('size_button');
const menuButton = document.querySelector('.menu_button');
const nameValue = document.getElementById('name_value');
const ul = document.getElementById('ul_top');
const title = document.querySelector('.title');
const save = document.getElementById('save_button');
const myStorage = window.localStorage;
let spacing = 12;
let score = 0;
let size = 4;
let width = (canvas.width- (size+1)*spacing)/size;
let menuVisible = false;
let cells = [];
let players = [];
let fontSize = 0;
let loss = false;
let won = false;
let moved = false;
let rotated = false;
startGame();

//Rozklikavani menu se jmenem a nejlepsima hracema
menuButton.addEventListener('click', toggleMenuState);
function toggleMenuState () {
    menuVisible = !menuVisible;
    if (menuVisible) {
        document.body.classList.add('menu-visible');
    } else {
        document.body.classList.remove('menu-visible');
    }
}
save.addEventListener('click', savePlayer);

//rotace ctverce 2048 pomoci jquery
$(document).ready(function() {
  $('.title').click(function() {
      $('.title').toggleClass('rotated');
  });
});

/*//rotace ctverce 2048 pomoci jen js
title.addEventListener('click', rotateTitle);
function rotateTitle(){
  if (!rotated){
    title.style.webkitTransform = 'rotate('+360+'deg)'; 
    title.style.mozTransform    = 'rotate('+360+'deg)'; 
    title.style.msTransform     = 'rotate('+360+'deg)'; 
    title.style.oTransform      = 'rotate('+360+'deg)'; 
    rotated = true; 
  }else{
    title.style.webkitTransform = 'rotate(-'+0+'deg)'; 
    title.style.mozTransform    = 'rotate(-'+0+'deg)'; 
    title.style.msTransform     = 'rotate(-'+0+'deg)'; 
    title.style.oTransform      = 'rotate(-'+0+'deg)'; 
    title.style.transform       = 'rotate(-'+0+'deg)';
    rotated = false;
  }
}*/

//Pro novou hru pomoci tlacitka start
changeSize.onclick = function () {
  newGame()
}
function newGame() {
    if (sizeInput.value >=4  && sizeInput.value <= 20) {
      score = 0;
      won = false;
      loss = false;
      document.getElementsByClassName("score_number")[0].innerHTML = score;
      size = parseInt(sizeInput.value);
      spacing = (36/size) + 3;
      width =  (canvas.width - (size+1)*spacing)/size;
      canvasClean();
      startGame();
    }
  }

  //Vycisteni Canvas plochy
  function canvasClean() {
    ctx.clearRect(0, 0, 520, 520);
  }

  //Metoda pro pohyb hry
  document.onkeydown = function (e) {
    if (!loss) {
      switch (e.keyCode) {
        case 37:
          moveLeft(false);
          break;
        case 38:
          moveUp(false);
          break;
        case 39:
          moveRight(false);
          break;
        case 40:
          moveDown(false);
          break;
      }
    }
  }
  
  //Vytvoreni jedne "bunky"
  function cell(row, coll) {
    this.value = 0;
    this.x = coll*width + spacing*(coll + 1) ;
    this.y = row*width + spacing*(row + 1);
  }
  
  //vytvoreni vsech bunek
  function createCells() {
    let i, j;
    for(i = 0; i < size; i++) {
      cells[i] = [];
      for(j = 0; j < size; j++) {
        cells[i][j] = new cell(i, j);
      }
    }
  }
  
  //Vykresleni jedne bunky
  function drawCell(cell) {
    ctx.beginPath();
    ctx.rect(cell.x, cell.y, width, width);
    switch (cell.value){
      case 0 : ctx.fillStyle = '#cdc1b4'; break;
      case 2 : ctx.fillStyle = '#eee3d6'; break;
      case 4 : ctx.fillStyle = '#ede0c8'; break;
      case 8 : ctx.fillStyle = '#f2b179'; break;
      case 16 : ctx.fillStyle = '#f59563'; break;
      case 32 : ctx.fillStyle = '#f67e5f'; break;
      case 64 : ctx.fillStyle = '#f65e3b'; break;
      case 128 : ctx.fillStyle = '#f1d96b'; break;
      case 256 : ctx.fillStyle = '#f2cf4d'; break;
      case 512 : ctx.fillStyle = '#e5c12b'; break;
      case 1024 : ctx.fillStyle = '#dfba12'; break;
      case 2048 : ctx.fillStyle = '#edc501'; break;
      case 4096 : ctx.fillStyle = '#ffbf00'; break;
      default : ctx.fillStyle = '#e63faa';
    }
    ctx.fill();

      fontSize = width / 2;
      ctx.font = fontSize + 'px Arial';
      ctx.textAlign = 'center';
    //rozliseni zda je bunka s hodnotou 2 nebo 4 pro rozdilne barvy
    if (cell.value == 2 || cell.value == 4 ) {
      ctx.fillStyle = '#776e65';
    }else if (cell.value){
        ctx.fillStyle = '#f9f6f2';
    }
    ctx.fillText(cell.value, cell.x + width / 2, cell.y + 9*width/14);
  }
  
  //zahajeni hry
  function startGame() {
    document.getElementsByClassName("score_number")[0].innerHTML = score;
    createArrayPlayers();
    clearLi();
    liMaker();
    createCells();
    drawAllCells();
    addNewCell();
    addNewCell();
  }
  
  //Vykresleni vsech bunek
  function drawAllCells() {
    let i, j;
    for(i = 0; i < size; i++) {
      for(j = 0; j < size; j++) {
        drawCell(cells[i][j]);
      }
    }
  }
  
  //pridani jedne bunky do hry
  function addNewCell() {    
    while(true) {
      let row = Math.floor(Math.random() * size);
      let coll = Math.floor(Math.random() * size);
      if(!cells[row][coll].value) {
        cells[row][coll].value = 2 * Math.ceil(Math.random() * 2);
        drawAllCells();
        return;
      }
    }
  }

  //atribut fake slouzi k tomu, aby se zjistilo, zda se jedna o skutecny pohyb ve hre, ci jen pro kontrolu vyhry/prohry
  //atribut moved slouzi k tomu, aby se zjistilo, zda se nejaka bunka na hraci plose posunula
  //obdobne u ostatnich pohybu
  //pohyb doprava
  function moveRight (fake) {
    moved = false;
    let i, j,addValue,coll;
    //prochazeni plochy
    for(i = 0; i < size; i++) {
      for(j = size - 2; j >= 0; j--) {
        //pokud je neco jineho nez 0
        if(cells[i][j].value) {
          //prochazeni po radcich/sloupech, zda se nahodou dve bunky neshoduji cislem
          for (coll = j; coll + 1 < size; coll++) {
            //kdyz bunka vedle je prazdna, pouze se posune
            if (!cells[i][coll + 1].value) {
              if(!fake){
                cells[i][coll + 1].value = cells[i][coll].value;
                cells[i][coll].value = 0;
              }
              moved = true;
            } 
            //pokud se bunky shoduji, spoji se, prida se skore
            else if (cells[i][coll].value == cells[i][coll + 1].value) {
              if(!fake){
                addValue = (cells[i][coll + 1].value *= 2);
                score +=  cells[i][coll + 1].value;
                cells[i][coll].value = 0;
                document.getElementsByClassName("score_number")[0].innerHTML = score;
              }
              moved = true;
              break;
            } else {
              break;
            }
          }
        }
      }
    }
    //kontrola vyhry prohry + pridani nove bunky
    afterMove(moved, fake, addValue);
    return moved;
    
  }
  
  //pohyb doleva
  function moveLeft(fake) {
    moved = false;
    let i, j,addValue,coll;
    for(i = 0; i < size; i++) {
      for(j = 1; j < size; j++) {
        if(cells[i][j].value) {
          coll = j;
          while (coll - 1 >= 0) {
            if (!cells[i][coll - 1].value) {
              if(!fake){
                cells[i][coll - 1].value = cells[i][coll].value;
                cells[i][coll].value = 0;
              }
              coll--; 
              moved = true;
              
            } else if (cells[i][coll].value == cells[i][coll - 1].value) {
              if(!fake){
                addValue = (cells[i][coll - 1].value *= 2);
                score += cells[i][coll - 1].value;
                cells[i][coll].value = 0;
                document.getElementsByClassName("score_number")[0].innerHTML = score;
              }
              moved = true;
              break;
            } else {
              break; 
            }
          }
        }
      }
    }
    afterMove(moved, fake, addValue);
    return moved;
  }
  
  //pohyb nahoru
  function moveUp(fake) {
    let i, j, row, addValue;
    moved = false;
    for(j = 0; j < size; j++) {
      for(i = 1; i < size; i++) {
        if(cells[i][j].value) {
          row = i;
          while (row > 0) {
            if(!cells[row - 1][j].value) {
              if(!fake){
                cells[row - 1][j].value = cells[row][j].value;
                cells[row][j].value = 0;
              }
              row--;
              moved = true;
            } else if (cells[row][j].value == cells[row - 1][j].value) {
              if(!fake){
                addValue = (cells[row - 1][j].value *= 2);
                score +=  cells[row - 1][j].value;
                cells[row][j].value = 0;
                document.getElementsByClassName("score_number")[0].innerHTML = score;
              }moved = true;
              break;
            } else {
                break;
            }
          }
        }
      }
    }
    afterMove(moved, fake, addValue);
    return moved;
  }
  
  //pohyb dolu
  function moveDown(fake) {
    let i, j, row, addValue;
    moved = false;
    for(j = 0; j < size; j++) {
      for(i = size - 2; i >= 0; i--) {
        if(cells[i][j].value) {
          row = i;
          while (row + 1 < size) {
            if (!cells[row + 1][j].value) {
              if(!fake){
                cells[row + 1][j].value = cells[row][j].value;
                cells[row][j].value = 0;
              }
              row++;
              moved = true;
            } else if (cells[row][j].value == cells[row + 1][j].value) {
              if(!fake){
                addValue = (cells[row + 1][j].value *= 2);
                score +=  cells[row + 1][j].value;
                cells[row][j].value = 0;
                document.getElementsByClassName("score_number")[0].innerHTML = score;
              }
              moved = true;
              break;
            } else {
              break;
            }
          }
          
        }
      }
    }
    afterMove(moved, fake, addValue);
    return moved;
  }

  //pridani nove bunky a kontrola vyhry/prohry za predpokladu, ze se nejedna o falesny posun a nejaka bunka se posunula
  function afterMove(moved, fake, addValue){
    if(moved && !fake){
        addNewCell();
        determinWin(addValue);
        determinLoss(); 
    }
  }
  
  //kontrola vyhry
  function determinWin(addvalue){
    if(addvalue == 2048 && won==false){
      won = true;
      new Audio("audio/win.mp3").play()
      setTimeout(()=>alert("Congratulation, you have created block with 2048!"), 200);
    }
  }

  //kontrola prohry
  function determinLoss(){
    let u = moveUp(true);
    let d = moveDown(true);
    let l = moveLeft(true);
    let r = moveRight(true);
    if(!u && !d && !l && !r){
      //savePlayer();
      new Audio("audio/lost.mp3").play();
      setTimeout(()=> alert("You lost"), 200);
      loss = true;
    }
  }

  //funkce na zaznamenani jmeno hrace a jeho vysledku, pokud je dostatecne uspesny, objevi se v tabulce
  function savePlayer(){
    let name = nameValue.value;
    //musi byt zadano jmeno
    if (!name){
      alert("You have to type a name!")
      return;
    }
    let exists = localStorage.getItem(name);
    //pokud neni v localStorage, konrola novych nejlepsich hracu
    if (!exists){
      topPlayers(name);
    }
    //pokud uz je, kontrola, zda nahral vic
    else{
      if (parseInt(exists) < score){
        localStorage.removeItem(name)
        localStorage.setItem(name, score);
        clearLi();
        liMaker();
      }
    }
  }

  //vycisti seznam hracu
  function clearLi(){
    while (ul.firstChild) {
      ul.removeChild(ul.firstChild)
    }
  }

  //vytvoreni arraye s hraci
  function createArrayPlayers(){
    let i, j;
    for(i = 0; i < 5; i++) {
      players[i] = [];
    }
  }

  //zapsani hracu do tabulky
  function liMaker() {
    //tragicky udelanej array s top hraci 
    for(let i = 0; i < localStorage.length; i++){
      players[i][0] = localStorage.key(i);
      players [i][1] = localStorage.getItem(localStorage.key(i));
    }
    //jeste tragictejsi pouziti bubbleSortu
    for(let i =0; i < localStorage.length-i; i++){
      for(let j =0; j < localStorage.length-i-1; j++){
        if(parseInt(players[j][1]) < parseInt(players[j+1][1])){
          let tmpName = players[j][0];
          let tmpScore = players [j][1];
          players[j][0]=players[j+1][0];
          players[j][1]=players[j+1][1];
          players[j+1][0]=tmpName;
          players[j+1][1]=tmpScore;
        }
      }
    }
    //appendovani jednotlivych hracu
    for(let i = 0; i < localStorage.length; i++){
      const li = document.createElement('li');
      li.textContent = players[i][0] +": "+ players[i][1];
      ul.appendChild(li);
    }
  }

  //zda hrac patri mezi elitu
  function topPlayers(name){
    //pokud je min jak 5 hracu automaticky je mezi toop 5
    if(localStorage.length <5){
      localStorage.setItem(name, score);
      clearLi();
      liMaker();
    }
    //pokud je vic jak 5 hracu, hledani nejhorsiho
    else{
      let min = 1000000;
      let nameMin = 6;
      for(let i = 0; i < 5; i++){
        let nameScore = localStorage.getItem(localStorage.key(i));
        if(parseInt(nameScore) < parseInt(min)){
          min = parseInt(nameScore);
          nameMin = i;
        }
      }
      //pokud patri mezi elitu, smazani nejhorsiho, pridani a kontrola poradi
      if (score>parseInt(min)){
        localStorage.removeItem(localStorage.key(nameMin));
        localStorage.setItem(name,score);
        clearLi();
        liMaker();
      }
    }
  }
  


  