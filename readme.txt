Cilem projektu bylo vytvoreni hry 2048, tedy hry, kde se spojuji bloky se stejnymi cisly. 
Hraje se pomoci sipek, pri tahu se vsechny bloky posunou ve stejnem smeru (smeru tahu) a 
pritom se spoji bloky se stejnymi cisly, ktere do sebe "narazi", a vytvori jeden blok s hodnotou
souctu techto dvou bloku, tedy z dvou bloku s hodnotou 16 se vytvori jeden blok s hodnotou 32,
z dvou bloku s hodnotou 8 se vytvori jeden s hodnotou 16 atd.

Popis funkcnosti:
Hra se tedy ovlada pomoci sipek, velikost pole se da nastavit pomoci pole pod "Score" 
(minimalni velikost je 4 a maximalni je 20) a hra se zahaji pomoci tlacitka Start. Skore se da ulozit
do tabulky nejlepsich hracu po otevreni menu, zadani jmena (musi byt zadano) a stisknuti tlacitka Save.
Hrac se promitne do tabulky pouze tehdy, je-li mezi 5-ti nejlepsimi hraci. Pokud jiz jmeno mezi nejlepsimi
hraci je, jeho skore se zaznamena pouze, je-li lepsi nez jeho puvodni.
Kliknuti na titulni ctverec s cisle 2048 zpusobi jeho krasne otoceni o 360�.